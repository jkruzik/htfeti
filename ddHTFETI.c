/**********************************************************************************************/
/* HTFETI                                                                                     */
/**********************************************************************************************/

/* Info and help */
static char help[] =
"This program generates objects for TFETI and HTFETI.\
Start: mpir -n 8 ./ddHTFETI -mh 8 -mhc 1 -dual_qp_E_orth_type implicit -dual_qp_E_orth_form implicit -dual_mat_inv_pc_factor_mat_solver_type mumps -dual_mat_inv_pc_type cholesky -dual_mat_inv_ksp_type preonly -dual_qppf_mat_inv_pc_factor_mat_solver_type superlu_dist -qps_smalxe_maxeig_iter 20 -qps_smalxe_M1_direct false -qps_smalxe_M1 1e2 -qps_smalxe_M1_update 10 -qps_smalxe_M2_active false -qps_smalxe_rho_update 1 -qps_smalxe_rho_update_late 2 -qps_smalxe_rho 1e2 -qps_smalxe_eta 1e-1 -qpt_dualize_B_nest_extension\
Options: -linear 1, -mview_sloc = compute schur complement
\n\n";


#include <permonqps.h>

PetscInt debug=0;

/* Definition of structure of parameters */
typedef struct
{
  PetscScalar   h  ;     /* h ... discretization parameter                         */
  PetscInt      rh ;     /* rh... number of subdomains in horizontal strips        */
  PetscInt      rv ;     /* rv... number of subdomains in vertical   strips        */
  PetscInt      mh ;     /* mh... number of nodes in a row of the subdomain        */
  PetscInt      mv ;     /* mv... number of nodes in a column of the subdomain     */
  PetscInt      rhc;     /* nhc...number of clusters in horizont dir.              */
  PetscInt      rvc;     /* rvc...number of clusters in vertic. dir.               */
  PetscInt      mhc;     /* nhc...number of subds in a row in cluster              */
  PetscInt      mvc;     /* rvc...number of subds in a column in cluster           */
  PetscInt      Niq;     /* Niq.. number of inequality conctrained multipliers     */
  PetscInt      Neq;     /* Neq.. number of equality conctrained multipliers       */
  PetscInt      Nprim;   /* Nprim ... global number of nodes (size of A)           */
  PetscInt      Ndual;   /* Ndual.size of dual problem                             */
  PetscInt      Nnull;   /* Nnull.size of vector e (= number of columns of R:Nrcol)*/
  PetscScalar   P1 ;     /* P1... density of forces on left  membrane              */
  PetscScalar   P2 ;     /* P2... density of forces on right membrane              */
  PetscInt      size;    /* size..size of communicator                             */
  PetscInt      rank;    /* rank..processor rank                                   */
  PetscInt      Nsubd;   /* Nsubd ...number of subdomains                          */
  PetscInt      Nclust;  /* Nclust...number of clusters                            */
  PetscInt      Nzero;   /* Nzero... number of gluing conditions of subds into clusters */
 } Param;


PetscErrorCode chessdd(Param *P,Mat *K,Mat *R,Mat *Bi,Mat *Be,Vec *F)
/*
  Chessboard domain decomposition of the model problem
  Discrete Laplace operator on O_1 U O_2
  Program generates sparse matrices K, B, R, f, ISiq

        *.............. ................
        *   P_1=-3    . .         O_1  .
        *.............. .              .
        *             . . G_c          .
    G_u-*   P_1=0     . .              .-G_f
        *             . .    P_2=0     .
        *             . ................
        *  O_2        . .    P_2=-1    .
        *.............. ................
           lower            upper

  Boundary conditions: u=0 on G_u, du/dn=0 on G_f, u_1<=u_2 on G_c
  Inside  O_1 U O_2  -delta u=f with f|O_i=P_i, i=1,2

  Created matrices:
  -----------------
  K ............ sparse stiffness matrix of the system
  B ............ matrix for evaluation of traces
  f ............ vector of forces
  R ............ kernel of A

  Notation: Nodes and subdomains are ordered columnwise
*/

{
  Mat Ko,BI,BE,Ro;
  Vec f;
  PetscInt     ierr, II, JJ, Istart, Iend, is, i, j, k, l, cols[5],rows[2];
  PetscScalar  vafirst[3], vainner[4], valast[3];
  PetscScalar  vbfirst[4], vbinner[5], vblast[4];
  PetscScalar  vrv[2], vlv[2];
  PetscScalar  one = 1.0, mone = -1.0, zero = 0.0;
  PetscScalar  c, fl, ff, cfl, cff;
  PetscInt     *nnz;
  PetscInt     lmax, lmin, lmvj, max, skip;
  PetscInt Iistart, Iiend;
  Mat BIperm, BEperm;
  IS  perm, nopermI, nopermE;
  PetscInt N=0;//, idx[P->Nprim+P->Nzero];
  PetscInt *Iclust0,*Iclust,*vpoz;
  PetscInt *idx;
	Mat Bjk,Bcon;
  PetscScalar *v,*Bjkzero;
  PetscReal *cnorms,*cnorms2;
  PetscInt  idxrc,*idjk,*idc, M,m,Nrows;
  PetscInt *idxjk;
  PetscInt Istart_loc,shift;
  IS  isc,isr,bjk;
  Vec rowsum;
  Mat *Bjkp;
  PetscLogStage stage_load,stage_assembly,stage_schur,stage_schure;

  PetscFunctionBegin;
  PetscLogStageRegister("Assembly", &stage_assembly);
  PetscLogStageRegister("Load", &stage_load);
  PetscLogStageRegister("Schur", &stage_schur);
  PetscLogStageRegister("Schure", &stage_schure);
  PetscLogStagePush(stage_load);
  /* TODO fix preallocation nnz K per row = 5 */
  ierr = PetscMalloc1((P->Nprim+P->Nzero)/P->Nclust,&nnz);CHKERRQ(ierr);
  for (i=0; i<P->Nprim/P->Nclust; i++) {
    nnz[i] = 6;
  }
  for (; i<(P->Nprim+P->Nzero)/P->Nclust; i++) {
    nnz[i] = 2*(P->mv-2);
  }
  //ierr = MatCreateSeqAIJ(PETSC_COMM_SELF,(P->Nprim+P->Nzero)/P->Nclust, (P->Nprim+P->Nzero)/P->Nclust, 2*(P->mv-2)+5, PETSC_NULLPTR,&Ko); CHKERRQ(ierr);
  ierr = MatCreateSeqAIJ(PETSC_COMM_SELF,(P->Nprim+P->Nzero)/P->Nclust, (P->Nprim+P->Nzero)/P->Nclust, 0, nnz,&Ko); CHKERRQ(ierr);
  ierr = PetscFree(nnz);CHKERRQ(ierr);
  ierr = MatCreateAIJ(PETSC_COMM_WORLD, PETSC_DECIDE, PETSC_DECIDE,             P->Nprim+P->Nzero, P->Niq,           2, PETSC_NULLPTR, 2, PETSC_NULLPTR, &BI);CHKERRQ(ierr);
  ierr = MatCreateAIJ(PETSC_COMM_WORLD, PETSC_DECIDE, PETSC_DECIDE,             P->Nprim+P->Nzero, P->Neq,           2, PETSC_NULLPTR, 2, PETSC_NULLPTR, &BE);CHKERRQ(ierr);
  ierr = MatCreateSeqAIJ(PETSC_COMM_SELF, (P->Nprim+P->Nzero)/P->Nclust,1, 1,PETSC_NULLPTR,&Ro); CHKERRQ(ierr);
  ierr = VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, P->Nprim+P->Nzero, &f);                                                                          CHKERRQ(ierr);

  ierr = MatSetOption(Ko, MAT_SYMMETRIC, PETSC_TRUE);    CHKERRQ(ierr);


  /*-----------------------------------------------------------------------*/
  /* Assembling the stiffness matrix A                                     */
  /*-----------------------------------------------------------------------*/
  /*
     There are stored 2*rh*rv/size subdomains on each of processors

     |  a -l  0              |  ... Local stiffness matrix for one subdomain
     | -l  b -l  0           |      First rv subdomains, that are fixed on the left site
     |  0 -l  b -l  0        |      have instead of first block   | a -l 0...0 |
     |       .  .  .  .  .   |      a block with identical matrix | l  0 0...0 |
     |         0 -l  b -l  0 |
     |            0 -l  b -l |          |  2 -1  0     |        |  3 -1  0     |
     |               0 -l  a |          | -1  3 -1  0  |        | -1  4 -1  0  |
                                    a = |    .  .  .  .|    b = |    .  .  .  .|
                		        |   0 -1  3 -1 |        |   0 -1  4 -1 |
                  		        |      0 -1  2 |        |      0 -1  3 |
  */
  vafirst[0] =  2.0; vafirst[1] = -1.0; vafirst[2] = -1.0;
  vainner[0] = -1.0; vainner[1] =  3.0; vainner[2] = -1.0; vainner[3] = -1.0;
  valast[0]  = -1.0; valast[1]  =  2.0; valast[2]  = -1.0;

  vbfirst[0] = -1.0; vbfirst[1] =  3.0; vbfirst[2] = -1.0; vbfirst[3] = -1.0;
  vbinner[0] = -1.0; vbinner[1] = -1.0; vbinner[2] =  4.0; vbinner[3] = -1.0; vbinner[4] = -1.0;
  vblast[0]  = -1.0; vblast[1]  = -1.0; vblast[2]  =  3.0; vblast[3]  = -1.0;


  //ierr = MatGetOwnershipRange(K, &Istart, &Iend); CHKERRQ(ierr);

  /* In one loop a subdomain's stiffness matrix is generated */

  II = 0;

  for ( is = 0; is < P->Nsubd/P->Nclust; is++ )
  {
    /* a -l */
    cols[0] = II; cols[1] = II + 1; cols[2] = II + P->mv;
    ierr = MatSetValues(Ko,1,&II,3,cols,vafirst,INSERT_VALUES); CHKERRQ(ierr);
    II++;

    for ( i = 1; i < P->mv - 1; i++ )
    {
      cols[0] = II - 1; cols[1] = II; cols[2] = II + 1; cols[3] = II + P->mv;
      ierr = MatSetValues(Ko,1,&II,4,cols,vainner,INSERT_VALUES); CHKERRQ(ierr);
      II++;
    }

    cols[0] = II - 1; cols[1] = II; cols[2] = II + P->mv;
    ierr = MatSetValues(Ko,1,&II,3,cols,valast,INSERT_VALUES); CHKERRQ(ierr);
    II++;

    /* -l b -l */
    cols[0] = II - P->mv; cols[1] = II; cols[2] = II + 1; cols[3] = II + P->mv;
    ierr = MatSetValues(Ko,1,&II,4,cols,vbfirst,INSERT_VALUES); CHKERRQ(ierr);
    II++;

    for ( i = 1; i < P->mv - 1; i++ )
    {
      cols[0] = II - P->mv; cols[1] = II - 1; cols[2] = II; 	cols[3] = II + 1; cols[4] = II + P->mv;
      ierr = MatSetValues(Ko,1,&II,5,cols,vbinner,INSERT_VALUES); CHKERRQ(ierr);
      II++;
    }

    cols[0] = II - P->mv; cols[1] = II - 1; cols[2] = II; cols[3] = II + P->mv;
    ierr = MatSetValues(Ko,1,&II,4,cols,vblast,INSERT_VALUES); CHKERRQ(ierr);
    II++;

    /* -l b -l */
    for ( j = 2; j < P->mh - 1; j++ )
    {
      cols[0] = II - P->mv; cols[1] = II; cols[2] = II + 1; cols[3] = II + P->mv;
      ierr = MatSetValues(Ko,1,&II,4,cols,vbfirst,INSERT_VALUES); CHKERRQ(ierr);
      II++;

      for ( i = 1; i < P->mv - 1; i++ )
      {
        cols[0] = II - P->mv; cols[1] = II - 1; cols[2] = II;	cols[3] = II + 1; cols[4] = II + P->mv;
        ierr = MatSetValues(Ko,1,&II,5,cols,vbinner,INSERT_VALUES); CHKERRQ(ierr);
        II++;
      }

      cols[0] = II - P->mv; cols[1] = II - 1; cols[2] = II; cols[3] = II + P->mv;
      ierr = MatSetValues(Ko,1,&II,4,cols,vblast,INSERT_VALUES); CHKERRQ(ierr);
      II++;

    }

    /* -l a */
    cols[0] = II; cols[1] = II + 1; cols[2] = II - P->mv;
    ierr = MatSetValues(Ko,1,&II,3,cols,vafirst,INSERT_VALUES); CHKERRQ(ierr);
    II++;

    for ( i = 1; i < P->mv - 1; i++ )
    {
      cols[0] = II - 1; cols[1] = II; cols[2] = II + 1; cols[3] = II - P->mv;
      ierr = MatSetValues(Ko,1,&II,4,cols,vainner,INSERT_VALUES); CHKERRQ(ierr);
      II++;
    }

    cols[0] = II - 1; cols[1] = II; cols[2] = II - P->mv;
    ierr = MatSetValues(Ko,1,&II,3,cols,valast,INSERT_VALUES); CHKERRQ(ierr);
    II++;

  }
  /*TODO assemble here */
  //ierr = MatAssemblyBegin(K,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  //ierr = MatAssemblyEnd(K,MAT_FINAL_ASSEMBLY);   CHKERRQ(ierr);

  //ierr = MatScale(K, 1.0 / P->h); CHKERRQ(ierr);

  //if (debug)
  //{
  //  if(!P->rank) ierr = MatView(K,PETSC_VIEWER_STDOUT_SELF); CHKERRQ(ierr);
  //  PetscPrintf(PETSC_COMM_WORLD,"Assembling of matrix A done\n");
  //}

  /*-----------------------------------------------------------------------*/
  /* Assembling the matrix R ... kernel of the stiffness matrix            */
  /*-----------------------------------------------------------------------*/
  //ierr = MatGetOwnershipRange(R, &Istart, &Iend); CHKERRQ(ierr);

  II = 0;
  Istart = 0; /* first column */

  for ( is = 0; is < P->Nsubd/P->Nclust; is++ )
  {
    for ( i = 0; i < P->mv * P->mh; i++ )
    {
      //ierr = MatSetValues(R,1,&II,1,&P->rank,&one,INSERT_VALUES); CHKERRQ(ierr);
      ierr = MatSetValues(Ro,1,&II,1,&Istart,&one,INSERT_VALUES); CHKERRQ(ierr);
      II++;
    }
  }

  ierr = MatAssemblyBegin(Ro,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(Ro,MAT_FINAL_ASSEMBLY);   CHKERRQ(ierr);

  //if (debug)
  //{
  //  ierr = MatView(R,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
  //  PetscPrintf(PETSC_COMM_WORLD,"Assembling of matrix R done\n");
  //}


 /*-----------------------------------------------------------------------*/
  /* Assembling the vector of forces f                                    */
  /*----------------------------------------------------------------------*/
  /*
     auxiliary functions fa1(j,mh,mv,rv) and fa2(j,mh,mv,rv)
     assign to each j=1,...,rv the distribution of forces in a column
     that belongs to the middle of any subdomain in the
     j-th row of O_i, i=1,2. In side strip uder traction fai(j)=1.
  */
  ierr = VecGetOwnershipRange(f,&Iistart,&Iiend);CHKERRQ(ierr);
  fl = P->P1 * P->h * P->h;
  ff = P->P2 * P->h * P->h;

  /* Normalized forces on the vertical strips */
  /*------------------------------------------*/
  II = 0;
  max = 1;
  if ( max < P->rv / 4 ) max = P->rv / 4;

  for (i = 1; i <= P->rh; i++)
  {
    for (j = 1; j <= P->rv; j++)
    {
      for (k = 1; k <= P->mh; k++)
      {
        /*
           function fa1
           if j<=max generate lmax nonzero entries of f else all mv entries of f
           are equal to zero
        */
        if (j <= max)
        {
          if ( k == 1 || k == P->mh )
            c = 0.5;
          else
	          c = 1.0;

          cfl = 0.5 * c * fl;

          lmax = P->mv;         /* lmax=min(mv,rv*(mv-1)/4-j*(mv-1)+mv) */
          lmvj = P->rv * (P->mv - 1) / 4 - j * (P->mv - 1) + P->mv;
          if ( lmax > lmvj ) lmax = lmvj;

          if (II>=Iistart && II<Iiend) {
            ierr = VecSetValues(f, 1, &II, &cfl, INSERT_VALUES); CHKERRQ(ierr);/* f(0)=c*0.5*fl */
          }
          II++;
	        cfl = cfl * 2;

          for (l=1; l<lmax-1; l++)                     /* f(l) = c*fl   */
          {
            if (II>=Iistart && II<Iiend) {
              ierr = VecSetValues(f, 1, &II, &cfl, INSERT_VALUES); CHKERRQ(ierr);
            }
            II++;
          }

	        cfl = cfl / 2;
          if (II>=Iistart && II<Iiend) {
            ierr = VecSetValues(f, 1, &II, &cfl, INSERT_VALUES);  CHKERRQ(ierr); /* f(lmax-1)=c*0.5*fl */
          }
          II++;
          II = II + P->mv - lmax;     /* skip mv-lmax zero entries */
        }
        else
        {
          II = II + P->mv;
        }
      }
    }
  }

  /* Normalized forces on the floating membrane */
  /*--------------------------------------------*/
  for (i = 1; i <= P->rh; i++)
  {
    for (j = 1; j <= P->rv; j++)
    {
      for (k = 1; k <= P->mh; k++)
      {
        /*
           function fa2
           if j>=3*rv/4+1 generate mv-lmin+1 nonzero entries of f else all mv entries of f
           are equal to zero
        */
        if (j >= 3 * P->rv / 4 + 1)
        {
          if ( k == 1 || k == P->mh )
	           c = 0.5;
	        else
	          c = 1.0;

          cff = 0.5 * c * ff;

          lmin = 1;            /* lmin=max(1,3*rv*(mv-1)/4-j*(mv-1)+mv) */
          lmvj = 3 * P->rv * (P->mv - 1) / 4 - j * (P->mv - 1) + P->mv;
          if ( lmin < lmvj ) lmin = lmvj;

          II = II + lmin - 1;                 /* skip lmin-1 zero entries */

          if (II>=Iistart && II<Iiend) {
            ierr = VecSetValues(f, 1, &II, &cff, INSERT_VALUES);  CHKERRQ(ierr); /* f(0)=c*0.5*ff */
          }
          II++;
	        cff = cff * 2;

          for (l = lmin; l < P->mv - 1; l++)           /* f(l) = c*ff   */
          {
            if (II>=Iistart && II<Iiend) {
              ierr = VecSetValues(f, 1, &II, &cff, INSERT_VALUES);	 CHKERRQ(ierr);
            }
            II++;
          }

          cff = cff / 2;
          if (II>=Iistart && II<Iiend) {
            ierr = VecSetValues(f, 1, &II, &cff, INSERT_VALUES);  CHKERRQ(ierr); /* f(lmax-1)=c*0.5*ff */
          }
          II++;
        }
        else
        {
          II = II + P->mv;
        }
      }
    }
  }

  ierr = VecAssemblyBegin(f); CHKERRQ(ierr);
  ierr = VecAssemblyEnd(f);   CHKERRQ(ierr);

  //if (debug)
  //{
  //  ierr = VecView(f,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
  //  PetscPrintf(PETSC_COMM_WORLD,"Assembling of forces f done\n");
  //}


  /*-----------------------------------------------------------------------*/
  /* Assembling the matrix B                                               */
  /*-----------------------------------------------------------------------*/
  /*
     number of columns of B (this matrix is transpose to theoretical matrix B)
     corresponds to size of dual problem

     Matrix:
     Biq describes noninterpenetration of the membranes on G_u
     Rs  picks the right column in subdomain
     Ls  picks the left column in  subdomain
     Rv  picks right columns in vertical strip
     Lv  picks the left columns in vertical strip
     Bh  describes continuity in horizontal direction, i.e. across the
         vertical interfaces. The matrices are empty when rh=1
         (no decomposition in horizontal direction)
     Ds  picks entries that are at the bottom (Down) of subdomains
     Us  picks upper entries of subdomains
     V   describes continuity in vertical strips

     Bv  describes continuity in vertical
 direction, i.e. on horizontal interfaces
         if rv=1 (no vertical decompositio) then the matrix is empty
     B   matrtix of constrains

     Noninterpenetration of the membranes on G_u:
     --------------------------------------------
     Biq=| 0  .  .  .   0   Rv   Lv   0  .  .  .   0 |
          \rv mv mh(rh-1)/           \rv mv mh(rh-1)/


     Continuity in horizontal strips:
     --------------------------------
         | 0 . . . 0 1 0 . . . 0 | \            | -1  0  .  0 0 . . . 0 | \
         | 0 . . . 0 0 1 0 . . 0 |              |  0 -1  0  0 0 . . . 0 |
     Rs =| . . . . 0 . . . . . 0 | mv       Ls =|  .  .  .  0 0 . . . 0 | mv
         | 0 . . . 0 0 0 0 . 1 0 |              |  0  0 -1  0 0 . . . 0 |
         | 0 . . . 0 0 0 0 . 0 1 | /            |  0  0  0 -1 0 . . . 0 | /
          \mv(mh-1)/ \    mv   /                   \   mv   / \mv(mh-1)/

         | Rs .  .  .  | \                      | Ls .  .  .  | \
         | .  Rs .  .  |                        | .  Ls .  .  |
     Rv =| .  .  .  .  | rv x Rs            Lv =| .  .  .  .  | rv x Ls
         | .  .  .  .  |                        | .  .  .  .  |
         | .  .  .  Rs | /                      | .  .  .  Ls | /
           \ rv mv mh /                           \ rv mv mh /

     Number of rows of matrices Rv and Lv after modification of interface and
     corner nodes is rv(mv-1)+1. Then

         | Rv  Lv  .   .  | \
         | .   Rv  Lv  .  |   rh-1 x =>
     Bh =| .   .   .   .  | rh(rv(mv-1)+1)
         | .   .   Rv  Lv | /
           \ rv rh mv mh /

     Continuity in vertical strips:
     ------------------------------
     d = | 0 . . . 0  1 |                   u = | -1  0 . . . 0 |
          \  mv-1  /                                  \  mv-1  /


         | d  .  .  .  | \                      | u  .  .  .  | \
         | .  d  .  .  |                        | .  u  .  .  |
     Ds =| .  .  .  .  | mh x d =>mh        Us =| .  .  .  .  | mh x u => mh
         | .  .  .  .  |                        | .  .  .  .  |
         | .  .  .  d  | /                      | .  .  .  u  | /
           \  mv mh  /                            \  mv mh  /

         | Ds  Us  .   .  | \
         | .   Ds  Us  .  |   rv-1 x =>
     V = | .   .   .   .  |   mh(rv-1)
         | .   .   Ds  Us | /
           \  rv mv mh  /

     Then
         | V   .   .   .  | \
         | .   V   .   .  |   rh x V =>
     Bv =| .   .   .   .  |   rh mh (rv-1)
         | .   .   .   V  | /
           \ rv rh mv mh /

     Matrix B:
     ---------
         |   Biq   |
         | Bh   .  |
     B = | .    Bh |
         | Bv   .  |
         | .    Bv |

     In this programm because of distribution of matrix across processors, matrix B has form of its transpose

  */
  ierr = MatGetOwnershipRange(BI, &Iistart, &Iiend); CHKERRQ(ierr);

  vrv[0] =  .5; vrv[1] = .5;
  vlv[0] = -.5; vlv[1] = -.5;
  one = .5*sqrt(2.); mone = -.5*sqrt(2.);

  skip = P->mv * (P->mh - 1);

  /* Noninterpenetration of the membranes - BI   */
  /*---------------------------------------------*/

  II = P->rv * P->mv * P->mh * (P->rh - 1);
  JJ = 0;

  II = II + skip;                 /* skip mv(mh-1) zeros in row of Rs */

  if( P->rv > 1 )
  {
    /* first Rs is generated */
    for ( i = 0; i < P->mv - 1; i++ )
    {
      if (II >= Iistart && II < Iiend) {
        ierr = MatSetValues(BI,1,&II,1,&JJ,&one,INSERT_VALUES); CHKERRQ(ierr);
      }
      II++;
      JJ++;
    }

    /* first corner node in vertical interface */
    rows[0] = II; rows[1] = II + skip + 1;
    if (II >= Iistart && II < Iiend) {
      ierr = MatSetValues(BI,2,rows,1,&JJ,vrv,INSERT_VALUES); CHKERRQ(ierr);
    }
    II++;
    JJ++;

    /* second - (rv-1)-th Rs is generated */
    for (j = 1; j < P->rv - 1; j++)
    {
      II = II + skip + 1;

      for ( i = 1; i < P->mv - 1; i++ )
      {

        if (II >= Iistart && II < Iiend) {
          ierr = MatSetValues(BI,1,&II,1,&JJ,&one,INSERT_VALUES); CHKERRQ(ierr);
        }
        II++;
        JJ++;
      }
      /* second - (rv-1)-th corner mode in vertical interface */
      rows[0] = II; rows[1] = II + skip + 1;
      if (II >= Iistart && II < Iiend) {
        ierr = MatSetValues(BI,2,rows,1,&JJ,vrv,INSERT_VALUES); CHKERRQ(ierr);
      }
      II++;
      JJ++;
    }

    /* rv-th Rs is generated */
    II = II + skip + 1;

    for ( i = 1; i < P->mv; i++ )
    {
      if (II >= Iistart && II < Iiend) {
        ierr = MatSetValues(BI,1,&II,1,&JJ,&one,INSERT_VALUES); CHKERRQ(ierr);
      }
      II++;
      JJ++;
    }

    /* return to first column */
    JJ = 0;

    /* first Ls is generated */
    for ( i = 0; i < P->mv - 1; i++ )
    {

      if (II >= Iistart && II < Iiend) {
        ierr = MatSetValues(BI,1,&II,1,&JJ,&mone,INSERT_VALUES); CHKERRQ(ierr);
      }
      II++;
      JJ++;
    }

    /* first corner mode in vertical interface */
    rows[0] = II; rows[1] = II + skip + 1;
    if (II >= Iistart && II < Iiend) {
      ierr = MatSetValues(BI,2,rows,1,&JJ,vlv,INSERT_VALUES); CHKERRQ(ierr);
    }
    II++;
    JJ++;

    /* second - (rv-1)-th Ls is generated */
    for (j = 1; j < P->rv - 1; j++)
    {
      II = II + skip + 1;

      for ( i = 1; i < P->mv - 1; i++ )
      {
        if (II >= Iistart && II < Iiend) {
          ierr = MatSetValues(BI,1,&II,1,&JJ,&mone,INSERT_VALUES); CHKERRQ(ierr);
        }
        II++;
        JJ++;
      }
      /* second - (rv-1)-th corner mode in vertical interface */
      rows[0] = II; rows[1] = II + skip + 1;
      if (II >= Iistart && II < Iiend) {
        ierr = MatSetValues(BI,2,rows,1,&JJ,vlv,INSERT_VALUES); CHKERRQ(ierr);
      }
      II++;
      JJ++;
    }

    /* rv-th Ls is generated */
    II = II + skip + 1;

    for ( i = 1; i < P->mv; i++ )
    {
      if (II >= Iistart && II < Iiend) {
        ierr = MatSetValues(BI,1,&II,1,&JJ,&mone,INSERT_VALUES); CHKERRQ(ierr);
      }
      II++;
      JJ++;
    }

  }
  else          /* no decomposition into horizontal strips */
  {
    /* one Rs is generated */
    for ( i = 0; i < P->mv; i++ )
    {
      if (II >= Iistart && II < Iiend) {
        ierr = MatSetValues(BI,1,&II,1,&JJ,&one,INSERT_VALUES); CHKERRQ(ierr);
      }
      II++;
      JJ++;
    }
    /* return to first column */
    JJ = 0;

    /* one Ls is generated */
    for ( i = 0; i < P->mv; i++ )
    {
      if (II >= Iistart && II < Iiend) {
        ierr = MatSetValues(BI,1,&II,1,&JJ,&mone,INSERT_VALUES); CHKERRQ(ierr);
      }
      II++;
      JJ++;
    }
  }

  ierr = MatAssemblyBegin(BI,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(BI,MAT_FINAL_ASSEMBLY);   CHKERRQ(ierr);

  //if (debug)
  //{
  //  ierr = MatView(BI,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
  //  PetscPrintf(PETSC_COMM_WORLD,"Assembling of matrix BI done\n");
  //}


  /* Parallel part of generation of matrix BE */
  /*------------------------------------------*/
  ierr = MatGetOwnershipRange(BE, &Iistart, &Iiend); CHKERRQ(ierr);
  //Istart = P->Nprim*P->rank/P->size;
  //Iend = P->Nprim*(P->rank+1)/P->size;
  Istart = 0;
  Iend = P->Nprim;

  /* Continuity in horizontal strips */
  /*---------------------------------*/

  if ( P->rh > 1 )             /* if rh==1 then matrix Bh is empty */
  {

    /* Matrices Rv are generated */
    /*---------------------------*/
    II = Istart;


    if ( Istart < P->Nprim / 2 )
      JJ = (Istart/(P->rv * P->mv * P->mh)) * P->Niq;  /* for left Bh */
    else                                         /* P->Niq=(rv*(mv-1)+1) */
      JJ = (Istart/(P->rv * P->mv * P->mh)-1) * P->Niq;  /* for floating Bh */

    /* In one loop matrix Rv is generated */

    while ( II >= Istart && II < Iend )
//    for ( is = 0; is < P->Nsubd/P->Nclust; is++ )
    {
      /*
         if all rv*mv*mh columns of matrix B are stored on one procesor
         then last submatrix in Bh is only Lv, which is generated in next part
      */
      if ( Istart == P->rv * P->mv * P->mh * (P->rh - 1) || Istart == P->rv * P->mv * P->mh * (2 * P->rh - 1) )  break;

      II = II + skip;                 /* skip mv(mh-1) zeros in row of Rs */

      /* first Rs is generated */
      for ( i = 0; i < P->mv - 1; i++ )
      {

        if (II >= Iistart && II < Iiend) {
          ierr = MatSetValues(BE,1,&II,1,&JJ,&one,INSERT_VALUES); CHKERRQ(ierr);
        }
        II++;
        JJ++;
      }

      /* first corner node in vertical interface */
      rows[0] = II; rows[1] = II + skip + 1;
      if (II >= Iistart && II < Iiend) {
        ierr = MatSetValues(BE,2,rows,1,&JJ,vrv,INSERT_VALUES); CHKERRQ(ierr);
      }
      II++;
      JJ++;

      /* second - (rv-1)-th Rs is generated */
      for (j = 1; j < P->rv - 1; j++)
      {
        II = II + skip + 1;

        for ( i = 1; i < P->mv - 1; i++ )
        {
          if (II >= Iistart && II < Iiend) {
            ierr = MatSetValues(BE,1,&II,1,&JJ,&one,INSERT_VALUES); CHKERRQ(ierr);
          }
          II++;
          JJ++;
        }
        /* second - (rv-1)-th corner mode in vertical interface */
        rows[0] = II; rows[1] = II + skip + 1;
        if (II >= Iistart && II < Iiend) {
          ierr = MatSetValues(BE,2,rows,1,&JJ,vrv,INSERT_VALUES); CHKERRQ(ierr);
        }
        II++;
        JJ++;
      }

      /* rv-th Rs is generated */
      II = II + skip + 1;

      for ( i = 1; i < P->mv; i++ )
      {
        if (II >= Iistart && II < Iiend) {
          ierr = MatSetValues(BE,1,&II,1,&JJ,&one,INSERT_VALUES); CHKERRQ(ierr);
        }
        II++;
        JJ++;
      }

      /*
         if last submatrix Rv of Bh is generated, then we will continue with
         I=I+rv*mv*mh  - if size==1 we will generate first Rv in second Bh,
         if size>1 then there is no Rv to generate and end (after adding I will
         be >Iend)
      */
      if ( II == P->rv * P->mv * P->mh * (P->rh - 1) ||  II == P->rv * P->mv * P->mh * (2 * P->rh - 1) )
        II = II + P->rv * P->mv * P->mh;

    }

    /* Matrices Lv are generated */
    /*---------------------------*/
    II = Istart;

    if ( Istart < P->Nprim / 2 )
      JJ = (Istart/(P->rv * P->mv * P->mh) - 1) * P->Niq;     /* for left Bh     */
    else
      JJ = (Istart/(P->rv * P->mv * P->mh) - 2) * P->Niq; /* for floating Bh */

    /* In one loop matrix Lv is generated */

    while ( II >= Istart && II < Iend )
//    for ( is = 0; is < P->Nsubd/P->Nclust; is++ )
    {
      if ( II == 0 )
      {
        II = II + P->rv * P->mv * P->mh;
        JJ = JJ + P->Niq;
        continue;
      }

      if ( II == P->Nprim / 2 )
      {
        II = II + P->rv * P->mv * P->mh;
        if ( P->size > 1 && Istart!=0)  JJ = JJ + P->Niq;
        continue;
      }

      /* first Ls is generated */
      for ( i = 0; i < P->mv - 1; i++ )
      {
        if (II >= Iistart && II < Iiend) {
          ierr = MatSetValues(BE,1,&II,1,&JJ,&mone,INSERT_VALUES); CHKERRQ(ierr);
        }
        II++;
        JJ++;
      }

      /* first corner mode in vertical interface */
      rows[0] = II; rows[1] = II + skip + 1;
      if (II >= Iistart && II < Iiend) {
        ierr = MatSetValues(BE,2,rows,1,&JJ,vlv,INSERT_VALUES); CHKERRQ(ierr);
      }
      II++;
      JJ++;

      /* second - (rv-1)-th Ls is generated */
      for (j = 1; j < P->rv - 1; j++)
      {
        II = II + skip + 1;

        for ( i = 1; i < P->mv - 1; i++ )
        {
          if (II >= Iistart && II < Iiend) {
            ierr = MatSetValues(BE,1,&II,1,&JJ,&mone,INSERT_VALUES); CHKERRQ(ierr);
          }
          II++;
          JJ++;
        }
        /* second - (rv-1)-th corner mode in vertical interface */
        rows[0] = II; rows[1] = II + skip + 1;

        if (II >= Iistart && II < Iiend) {
          ierr = MatSetValues(BE,2,rows,1,&JJ,vlv,INSERT_VALUES); CHKERRQ(ierr);
        }
        II++;
        JJ++;
      }

      /* rv-th Ls is generated */
      II = II + skip + 1;

      for ( i = 1; i < P->mv; i++ )
      {
        if (II >= Iistart && II < Iiend) {
          ierr = MatSetValues(BE,1,&II,1,&JJ,&mone,INSERT_VALUES); CHKERRQ(ierr);
        }
        II++;
        JJ++;
      }

      II = II + skip;               /* skip mv(mh-1) zeros in row of Ls */
    }
  }

  /* Continuity in vertical strips */
  /*-------------------------------*/
  if ( P->rv > 1 )                /* if rv==1 then matrix Bv is empty */
  {
    /* Matrices Ds are generated */
    /*---------------------------*/
    II = Istart + P->mv - 1;
    /*
       starting column - number of previous V matrices times size(V,1) plus
       Biq' and 2*Bh'
    */
    JJ = (Istart / (P->rv * P->mv * P->mh)) * P->mh * (P->rv - 1) +  P->Niq * 2 * (P->rh - 1);

    /* In one loop matrix Ds is generated */
    while ( II >= Istart && II < Iend )
//    for ( is = 0; is < P->Nsubd/P->Nclust; is++ )
    {
      /* one Ds is generated */
      for ( j = 0; j < P->rv - 1; j++ )
      {
        for ( i = 0; i < P->mh; i++ )
        {
          if (II >= Iistart && II < Iiend) {
            ierr = MatSetValues(BE,1,&II,1,&JJ,&one,INSERT_VALUES); CHKERRQ(ierr);
          }
          II = II + P->mv;
          JJ++;
        }
      }

      II = II + P->mv * P->mh;  /* return back and skip one matrix Us */
    }

    /* Matrices Us are generated */
    /*---------------------------*/
    II = Istart + P->mv * P->mh;                    /* skip matrix Us */
    /*
       starting column - number of previous V matrices minus 1 times size(V,1)
       plus Biq' and 2*Bh' - 2*rv*(rh-1)*mv+rv*mv
    */
    JJ = (Istart / (P->rv * P->mv * P->mh)) * P->mh * (P->rv - 1) + P->Niq * 2 * (P->rh - 1);

    /* In one loop matrix Us is generated */
    while ( II >= Istart && II < Iend )
//    for ( is = 0; is < P->Nsubd/P->Nclust; is++ )
    {
      /* one Us is generated */
      for ( j = 0; j < P->rv - 1; j++ )
      {
        for ( i = 0; i < P->mh; i++ )
        {
          if (II >= Iistart && II < Iiend) {
            ierr = MatSetValues(BE,1,&II,1,&JJ,&mone,INSERT_VALUES); CHKERRQ(ierr);
          }
          II = II + P->mv;
          JJ++;
        }
      }

      II = II + P->mv * P->mh;                  /* skip one matrix Ds */
    }
  }


  /* Dirichlet boundary conditions*/
  /*------------------------------*/

  II = Istart;
  JJ = P->Neq-P->Niq-(P->rv-1);

  one = 1.;
  while(II >= Istart && II < Iend)
//  for ( is = 0; is < P->Nsubd/P->Nclust; is++ )
  {
    if(II < P->rv * P->mv * P->mh)
    {
      for(i=0;i<P->mv;i++)
      {
        if (II >= Iistart && II < Iiend) {
          ierr = MatSetValues(BE,1,&II,1,&JJ,&one,INSERT_VALUES); CHKERRQ(ierr);
        }
        II++;
        JJ++;
      }
    }
    II = II + (P->mh - 1) * P->mv;
  }

  ierr = MatAssemblyBegin(BE,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(BE,MAT_FINAL_ASSEMBLY);   CHKERRQ(ierr);

  //if (debug)
  //{
  //  ierr = MatView(BE,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
  //  PetscPrintf(PETSC_COMM_WORLD,"Assembling of matrix BE done\n");
  //}
if (P->Nzero) {
  //==============================================================================================================
  //PetscInt  Iclust0[P->rhc][P->rvc][P->mhc*P->mvc], Iclust[P->Nclust][P->mhc*P->mvc], vpoz[P->Nsubd+1];    //Iclust - lists of subdomains creating clusters
  ierr = PetscMalloc1(P->rhc*P->rvc*P->mhc*P->mvc,&Iclust0);CHKERRQ(ierr);
  ierr = PetscMalloc1(P->Nclust*P->mhc*P->mvc,&Iclust);CHKERRQ(ierr);
  ierr = PetscMalloc1(P->Nsubd+1,&vpoz);CHKERRQ(ierr);
  vpoz[0]=0;

  for (i=1; i<=P->Nsubd; i++)
  {
     vpoz[i]=vpoz[i-1]+P->mh*P->mv;
  }

  for (i=0; i<P->rhc; i++)
  {
      for (j=0; j<P->rvc; j++)
      {
          for (k=0; k<P->mhc; k++)
          {
              for (l=0; l<P->mvc; l++)
              {
                 //Iclust0[i][j][k*P->mvc+l]=i*P->rvc*P->mhc*P->mvc+j*P->mvc+k*P->rv+l;
                 Iclust0[(i*P->rvc+j)*P->mhc*P->mvc + k*P->mvc+l]=i*P->rvc*P->mhc*P->mvc+j*P->mvc+k*P->rv+l;
              }
          }
      }
  }

  for (i=0; i<P->rhc; i++)
  {
      for (j=0; j<P->rvc; j++)
      {
          for (k=0; k<P->mhc*P->mvc; k++)
          {
              //Iclust[i*P->rvc+j][k]=Iclust0[i][j][k];
              //Iclust[(i+P->rhc)*P->rvc+j][k]=Iclust0[i][j][k]+P->rv*P->rh;
              Iclust[(i*P->rvc+j)*P->mhc*P->mvc+k]=Iclust0[(i*P->rvc+j)*P->mhc*P->mvc+k];
              Iclust[((i+P->rhc)*P->rvc+j)*P->mhc*P->mvc+k]=Iclust0[(i*P->rvc+j)*P->mhc*P->mvc+k]+P->rv*P->rh;
          }
      }
  }
  ierr = PetscFree(Iclust0);CHKERRQ(ierr);

  ierr = MatGetOwnershipRange(BE, &Istart, &Iend); CHKERRQ(ierr);
  ierr = PetscMalloc1(Iend-Istart,&idx);CHKERRQ(ierr);
  i=P->rank;
  N=Istart;

     for (j=0; j<P->mhc*P->mvc; j++)
     {
        for (k=vpoz[Iclust[i*P->mhc*P->mvc +j]]; k<vpoz[Iclust[i*P->mhc*P->mvc +j]+1]; k++)
        {
              idx[N-Istart]=k;
              N++;
        }

     }
  ierr = PetscFree(Iclust);CHKERRQ(ierr);
  ierr = PetscFree(vpoz);CHKERRQ(ierr);
     for (k=P->Nprim+i*P->Nzero/P->size; k<P->Nprim+(i+1)*P->Nzero/P->size; k++)
     {
           idx[N-Istart]=k;
           N++;
     }

  //ierr = ISCreateGeneral(PETSC_COMM_WORLD, P->Nprim+P->Nzero, idx, PETSC_COPY_VALUES, &perm);   CHKERRQ(ierr);
  ierr = ISCreateGeneral(PETSC_COMM_WORLD, Iend-Istart, idx, PETSC_COPY_VALUES, &perm);   CHKERRQ(ierr);
  ierr = MatGetOwnershipRangeColumn(BI, &Istart, &Iend); CHKERRQ(ierr);
  //ierr = ISCreateStride(PETSC_COMM_WORLD, P->Niq, 0, 1, &nopermI);     CHKERRQ(ierr);
  ierr = ISCreateStride(PETSC_COMM_WORLD, Iend-Istart, Istart, 1, &nopermI);     CHKERRQ(ierr);
  //ierr = ISCreateStride(PETSC_COMM_WORLD, P->Neq, 0, 1, &nopermE);     CHKERRQ(ierr);
  ierr = MatGetOwnershipRangeColumn(BE, &Istart, &Iend); CHKERRQ(ierr);
  ierr = ISCreateStride(PETSC_COMM_WORLD, Iend-Istart, Istart, 1, &nopermE);     CHKERRQ(ierr);

 Mat Bt;
 //MatTranspose(BE,MAT_INITIAL_MATRIX,&Bt);
 //       MatView(Bt,PETSC_VIEWER_STDOUT_WORLD);
  ierr = MatPermute(BI, perm, nopermI, &BIperm);   CHKERRQ(ierr);
  ierr = MatPermute(BE, perm, nopermE, &BEperm);   CHKERRQ(ierr);
  //ierr = VecPermute(f, perm, PETSC_FALSE);         CHKERRQ(ierr); /* doesn't support parallel */
  if (debug) {
    PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n%s\n-------------------------------\n","perm IS");
    ISView(perm,PETSC_VIEWER_STDOUT_WORLD);
    PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n%s\n-------------------------------\n","f");
    VecView(f,PETSC_VIEWER_STDOUT_WORLD);
  }
	PetscScalar *arrf;
  Vec fperm;
  VecScatter scatter;
  ierr = VecDuplicate(f,&fperm);CHKERRQ(ierr);
  //ierr = VecGetOwnershipRange(f, &Istart, &Iend); CHKERRQ(ierr);
  //ierr = VecGetArray(f,&arrf);CHKERRQ(ierr);
  //ierr = VecSetValues(fperm,Iend-Istart,idx,arrf,INSERT_VALUES);CHKERRQ(ierr);
  //ierr = VecAssemblyBegin(fperm);CHKERRQ(ierr);
  //ierr = VecAssemblyEnd(fperm);CHKERRQ(ierr);
  //ierr = VecRestoreArray(f,&arrf);CHKERRQ(ierr);
  ierr = PetscFree(idx);CHKERRQ(ierr);
  ierr = VecZeroEntries(fperm);CHKERRQ(ierr);
  ierr = VecScatterCreate(f,NULL,fperm,perm,&scatter);CHKERRQ(ierr);
  ierr = VecScatterBegin(scatter,f,fperm,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(scatter,f,fperm,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = ISDestroy(&perm);CHKERRQ(ierr);
  ierr = VecScatterDestroy(&scatter);CHKERRQ(ierr);
  ierr = VecDestroy(&f);CHKERRQ(ierr);
  f = fperm;
  if (debug) {
    PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n%s\n-------------------------------\n","f perm");
    VecView(f,PETSC_VIEWER_STDOUT_WORLD);
    MatTranspose(BE,MAT_INITIAL_MATRIX,&Bt);
    PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n%s\n-------------------------------\n","BE");
    MatView(Bt,PETSC_VIEWER_STDOUT_WORLD);
    MatDestroy(&Bt);
    MatTranspose(BEperm,MAT_INITIAL_MATRIX,&Bt);
    PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n%s\n-------------------------------\n","BE perm");
    MatView(Bt,PETSC_VIEWER_STDOUT_WORLD);
    MatDestroy(&Bt);
    MatTranspose(BI,MAT_INITIAL_MATRIX,&Bt);
    PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n%s\n-------------------------------\n","BI");
    MatView(Bt,PETSC_VIEWER_STDOUT_WORLD);
    MatDestroy(&Bt);
    MatTranspose(BIperm,MAT_INITIAL_MATRIX,&Bt);
    PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n%s\n-------------------------------\n","BI perm");
    MatView(Bt,PETSC_VIEWER_STDOUT_WORLD);
    MatDestroy(&Bt);
    PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n%s\n-------------------------------\n","R rank 0");
    if(!P->rank) MatView(Ro,PETSC_VIEWER_STDOUT_SELF);
  }
  ierr = MatDestroy(&BI);                          CHKERRQ(ierr);
  ierr = MatDestroy(&BE);                          CHKERRQ(ierr);
  ierr = ISDestroy(&nopermI);                      CHKERRQ(ierr);
  ierr = ISDestroy(&nopermE);                      CHKERRQ(ierr);

  BI   = BIperm;               /* Replace original operator with permuted version */
  BE   = BEperm;               /* Replace original operator with permuted version */

  ierr = VecGetOwnershipRange(f, &Istart, &Iend); CHKERRQ(ierr);
  ierr = PetscMalloc1(2*(P->mh-2),&Bjkzero);CHKERRQ(ierr);
  ierr = PetscMalloc2(P->Neq,&cnorms,P->Neq,&cnorms2);CHKERRQ(ierr);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*(P->mh+1)*(P->mv+1),&idjk);CHKERRQ(ierr);CHKERRQ(ierr);
  ierr = PetscMalloc1(P->Neq,&idc);CHKERRQ(ierr);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*(P->mh-2),&idxjk);CHKERRQ(ierr);CHKERRQ(ierr);
  M = (P->mvc-1)*P->mhc + P->mvc*(P->mhc-1); /* number of gluing averages rows to add to K */
  i = P->rank;
  // for i=1:size(Iclust,1)  ... one core corresponds to one cluster
   for (j=0; j<P->mhc*P->mvc; j++)
   {
      for (k=j+1; k<P->mhc*P->mvc; k++)
      {

        N=0;
        //Istart_loc = Istart + j*P->mh*P->mv;
        //shift = i*((P->mvc-1)*P->mhc + P->mvc*(P->mhc-1)); /* number interfaces added by previous clusters */
        //for (l=shift+vpoz[Iclust[i][j]]; l<shift+vpoz[Iclust[i][j]+1]; l++)
        //{
        //  if (l != Istart_loc && l != Istart_loc+P->mh-1 && l != Istart_loc+P->mh*(P->mv-1) && l != Istart_loc+P->mv*P->mh-1) {
        //     idjk[N]=l;
        //     N++;
        //  }
        //}
        //Istart_loc = Istart + k*P->mh*P->mv;
        //for (l=shift+vpoz[Iclust[i][k]]; l<shift+vpoz[Iclust[i][k]+1]; l++)
        //{
        //  if (l != Istart_loc && l != Istart_loc+P->mh-1 && l != Istart_loc+P->mh*(P->mv-1) && l != Istart_loc+P->mv*P->mh-1) {
        //   idjk[N]=l;
        //   N++;
        //  }
        //}
        for (l=0; l<P->mh*P->mv; l++)
        {
          if (l != 0 && l != (P->mv-1) && l != P->mh*(P->mv-1) && l != (P->mv*P->mh-1)) {
             idjk[N]=Istart + j*P->mh*P->mv+l;
             //if (!i) printf("j: %d %d %d\n",j,l,idjk[N]);
             N++;
					}
        }
        for (l=0; l<P->mh*P->mv; l++)
        {
          if (l != 0 && l != (P->mv-1) && l != P->mh*(P->mv-1) && l != (P->mv*P->mh-1)) {
            idjk[N]=Istart + k*P->mh*P->mv+l;
            N++;
					}
        }
        Nrows = N;
        //for (l=0; l<N; l++) {
        //  if(!i) printf("idjk: %d\n",idjk[l]);
        //  }
        ierr = ISCreateGeneral(PETSC_COMM_SELF, N, idjk, PETSC_COPY_VALUES, &bjk);   CHKERRQ(ierr);
        ierr = ISCreateStride(PETSC_COMM_SELF, P->Neq,0,1,&isc);   CHKERRQ(ierr);
        ierr = MatCreateSubMatrices(BE, 1, &bjk, &isc, MAT_INITIAL_MATRIX,&Bjkp);              CHKERRQ(ierr);
        ierr = ISDestroy(&isc);CHKERRQ(ierr);
        //Mat Bt;
        //MatTranspose(BE,MAT_INITIAL_MATRIX,&Bt);
        //MatView(BE,PETSC_VIEWER_STDOUT_WORLD);
        //MatDestroy(&Bt);
        //MatTranspose(Bjk,MAT_INITIAL_MATRIX,&Bt);
        //if (!i) MatView(*Bjkp,PETSC_VIEWER_STDOUT_SELF);
        //MatDestroy(&Bt);
        //ierr = MatGetColumnNorms(Bjk,NORM_INFINITY,cnorms);CHKERRQ(ierr);
        ierr = MatGetColumnNorms(*Bjkp,NORM_1,cnorms2);CHKERRQ(ierr);
        N=0;
        for (l=0; l<P->Neq; l++) {

          if (PetscAbsReal(sqrt(2.)-cnorms2[l]) < PETSC_SMALL) { /* colunm connects exactly two DOFs */
           idc[N] = l;
           //if (!i) printf("%d: %d %f\n",N,l,cnorms2[l]);
            N++;
          }
        }
        ierr = ISCreateGeneral(PETSC_COMM_SELF, N,idc,PETSC_COPY_VALUES,&isc);   CHKERRQ(ierr);
        ierr = ISCreateStride(PETSC_COMM_SELF, Nrows,0,1,&isr);   CHKERRQ(ierr);
        ierr = MatCreateSubMatrix(*Bjkp,isr,isc,MAT_INITIAL_MATRIX,&Bcon);              CHKERRQ(ierr);
        ierr = ISDestroy(&isc);CHKERRQ(ierr);
        ierr = ISDestroy(&isr);CHKERRQ(ierr);
        ierr = ISDestroy(&bjk);   CHKERRQ(ierr);
        ierr = MatDestroySubMatrices(1,&Bjkp);CHKERRQ(ierr);
        ierr = MatCreateVecs(Bcon,NULL,&rowsum);CHKERRQ(ierr);
        ierr = MatGetRowSum(Bcon,rowsum);CHKERRQ(ierr);
      //VecView(rowsum,PETSC_VIEWER_STDOUT_WORLD);
        ierr = MatDestroy(&Bcon);CHKERRQ(ierr);
        ierr = VecGetArray(rowsum,&v);CHKERRQ(ierr);
        N=0;
        for (l=0; l<Nrows; l++) {
          if (PetscAbsReal(v[l]) > PETSC_SMALL) { /* nonzero */
            Bjkzero[N] = v[l];
            idxjk[N] = idjk[l]-Istart; /* map back to the original local numbering */
            //if (!i) printf("%d: %d %f\n",P->rank,idxjk[N],v[l]);
            N++;
          }
        }
        ierr = VecRestoreArray(rowsum,&v);CHKERRQ(ierr);
        ierr = VecDestroy(&rowsum);CHKERRQ(ierr);

        //Bjkzero=Bjkzero/norm(Bjkzero);

        //if(!i) printf("%N:%d\n",N);
        if (N) {
          PERMON_ASSERT(N == 2*(P->mv-2), "Number of gluing DOFs %d != %d expected gluing DOFs)",N,2*(P->mv-2));
          idxrc = Iend-Istart -M;
					//if(!i) printf("%d: M=%d\n",i,M);
          ierr = MatSetValues(Ko,1,&idxrc,N,idxjk,Bjkzero,INSERT_VALUES); CHKERRQ(ierr);
          ierr = MatSetValues(Ko,N,idxjk,1,&idxrc,Bjkzero,INSERT_VALUES); CHKERRQ(ierr);  // je to nutne, kdyz K je symetricka?
          M--;
        }
      }
   }
  ierr = PetscFree(Bjkzero);CHKERRQ(ierr);
  ierr = PetscFree2(cnorms,cnorms2);CHKERRQ(ierr);CHKERRQ(ierr);
  ierr = PetscFree(idjk);CHKERRQ(ierr);CHKERRQ(ierr);
  ierr = PetscFree(idc);CHKERRQ(ierr);CHKERRQ(ierr);
  ierr = PetscFree(idxjk);CHKERRQ(ierr);CHKERRQ(ierr);
}
   ierr = MatAssemblyBegin(Ko,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
   ierr = MatAssemblyEnd(Ko,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
     PetscBool flg;
     KSP ksp;
     PetscCall(PetscOptionsHasName(NULL,NULL,"-mview_sloc",&flg));
     if (flg) {
        PetscLogStagePush(stage_schur);
        IS isa,isb,isc;
        PetscInt *ib;
        Mat Sa,Sp;
        PetscCall(PetscMalloc1(4*(P->mv-1),&ib));
        for (i=0; i<P->mv; i++) {
          ib[i] = i;
        }

        j = P->mv;
        k = P->mv;
        for (i=1; i<P->mh-1; i++) {
          ib[j] = k;
          k += P->mv;
          ib[j+1] = k-1;
          j += 2;
        }
        for (i=k; i<P->mv*P->mh; i++) {
          ib[j] = i;
          j++;
        }

        PetscCall(ISCreateGeneral(PETSC_COMM_SELF,4*(P->mv-1),ib,PETSC_OWN_POINTER,&isb));
        PetscCall(ISCreateStride(PETSC_COMM_SELF,P->mv*P->mh,0,1,&isa));
        PetscCall(ISDifference(isa,isb,&isc));
        PetscCall(MatGetSchurComplement(Ko,isc,isc,isb,isb,MAT_INITIAL_MATRIX,&Sa,MAT_SCHUR_COMPLEMENT_AINV_DIAG,MAT_IGNORE_MATRIX,NULL));
        PetscCall(MatSchurComplementGetKSP(Sa,&ksp));
        PetscCall(KSPSetOptionsPrefix(ksp,"schur_"));
        PetscCall(KSPSetFromOptions(ksp));
        PetscLogStagePush(stage_schure);
        PetscCall(MatSchurComplementComputeExplicitOperator(Sa,&Sp));
        PetscLogStagePop();
   if (P->rank==0) {
        PetscCall(MatViewFromOptions(Sp,NULL,"-mview_sloc"));
  }
        PetscCall(ISDestroy(&isa));
        PetscCall(ISDestroy(&isb));
        PetscCall(ISDestroy(&isc));
        PetscLogStagePop();
  }

        //MatViewFromOptions(Ko,NULL,"-mview_sloc");

   if (debug){
      PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n%s\n-------------------------------\n","K rank 1");
      if (i==1) MatView(Ko,PETSC_VIEWER_STDOUT_SELF);
   }
//if (!P->rank) MatCheckNullSpace(K,R,1e-10);
  //end

  //==============================================================================================================
  /*
  BB=B;
  for l=size(BB,1):-1:1
     if nnz(BB(l,:))==4
        BB(l,:)=[];
     end
  end

  for i=1:size(Iclust,1)
      Bzero_clust =[];
      Bzero=[];
      n=1;
      for j=1:size(Iclust,2)
          for k=j+1:size(Iclust,2)
              Bj=BB(:,vpoz(Iclust(i,j))+1:vpoz(Iclust(i,j)+1)); Bk=BB(:,vpoz(Iclust(i,k))+1:vpoz(Iclust(i,k)+1));
              Bjk=[Bj Bk];
              Bjkzero=zeros(1,size(Bjk,2)); m=0;
              for l=1:size(Bjk,1)
                  if nnz(Bjk(l,:))==2
                      Bjkzero=Bjkzero+Bjk(l,:);
                      m=m+1;
                  end
              end

              if nnz(Bjkzero)>0
                  Bzero(n,[vpoz(Iclust(i,j))+1:vpoz(Iclust(i,j)+1) vpoz(Iclust(i,k))+1:vpoz(Iclust(i,k)+1)])=Bjkzero/norm(Bjkzero);
                  n=n+1;
              end
          end
          if (size(Bzero_clust,1)<size(Bzero,1) & j>1) Bzero_clust(size(Bzero,1),size(Bzero_clust,2))=0; end
          Bzero_clust =[Bzero_clust     Bzero(:,vpoz(Iclust(i,j))+1:vpoz(Iclust(i,j)+1))];
      end
  end
  */
  //==============================================================================================================

  *Be = BE;
  *Bi = BI;
  *F = f;
  *K = Ko;
  *R = Ro;
  ierr = PetscLogStagePop();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

//---------------------------------------------------------------------------------------------------
int main(int argc,char **args)
{
  Mat           K;           /* global stiffness matrix            */
  Mat           Bi, Be;      /* matrix with inequality and equality constraints */
  Mat           R;           /* null space of K                    */
  Vec           F;           /* vector of forces                   */
  Param         P;           /* structure of parameters            */
  PetscInt      ierr;
  PetscBool     flg,linear=PETSC_FALSE;

  ierr = PermonInitialize(&argc,&args,(char *)0,help);if (ierr) return ierr;
  MPI_Comm_rank(PETSC_COMM_WORLD,&P.rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&P.size);
  //FLLOP_ASSERT(sqrt(P.size)%2.==0., "Number of ranks has to be even");

  //ierr = PetscOptionsGetInt(PETSC_NULLPTR, PETSC_NULLPTR, "-rh" , &P.rh , &flg);          CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULLPTR, PETSC_NULLPTR, "-mh" , &P.mh , &flg);          CHKERRQ(ierr);
  //ierr = PetscOptionsGetInt(PETSC_NULLPTR, PETSC_NULLPTR, "-rhc", &P.rhc, &flg);          CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULLPTR, PETSC_NULLPTR, "-mhc", &P.mhc, &flg);          CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(PETSC_NULLPTR, PETSC_NULLPTR, "-linear", &linear, NULL);          CHKERRQ(ierr);

  P.mh = P.mh+1; /* treat mh parameter as number of elements to be consistent with HTBETI implmentation */

  P.rhc = sqrt(P.size/2);
  P.P1 = -3.0;    P.P2 = -1.0;
  P.rh = P.rhc*P.mhc;
  P.h  = 1.0/((P.mh-1)*P.rh);
  P.rv = P.rh;
  P.mv = P.mh;
  P.rvc = P.rhc;
  P.mvc = P.mhc;
  P.Niq= P.rv * (P.mv - 1) + 1;
  //P.Niq= P.rv * P.mv + 1;
  P.Nprim = 2 * P.rv * P.rh * P.mv * P.mh;
  //P.Ndual = (2 * P.rh - 1) * (P.rv * (P.mv - 1) + 1) + 2 * P.rh * P.mh * (P.rv - 1) + P.Niq + 1;  // +P.Niq+1 ... Dirichlet bc
  P.Ndual = (2 * P.rh - 1) * (P.rv * (P.mv - 1) + 1) + 2 * P.rh * P.mh * (P.rv - 1) + P.Niq + (P.rv-1);  // +P.Niq+(P->rv-1) ... Dirichlet bc
  P.Neq = P.Ndual - P.Niq;
  P.Nsubd = 2 * P.rv * P.rh;
  P.Nclust = 2 * P.rvc * P.rhc;
  P.Nnull = P.Nclust;
  P.Nzero = 2* ( P.mhc - 1 ) * P.mvc * P.Nclust;
  PetscLogStage stage_transform,stage_solve;

  PetscLogStageRegister("Transform", &stage_transform);
  PetscLogStageRegister("Solve", &stage_solve);

  PetscPrintf(PETSC_COMM_WORLD,"Parameters   : rh=%d,rv=%d,rhc=%d,rvc=%d,mh=%d,mv=%d,mhc=%d,mvc=%d\n", P.rh, P.rv, P.rhc, P.rvc, P.mh, P.mv, P.mhc, P.mvc);
  PetscPrintf(PETSC_COMM_WORLD,"Nzero=%d,size=%d\n",P.Nzero,P.size);
  PetscPrintf(PETSC_COMM_WORLD,"Nprim=%d,Ndual=%d,Nnull=%d,Niq=%d,Neq=%d\n\n",P.Nprim, P.Ndual, P.Nnull, P.Niq, P.Neq);

  ierr = chessdd(&P, &K, &R,&Bi,&Be,&F);CHKERRQ(ierr);
  {
    Mat Kbd,Rbd,Bet,Bit;
    Vec            ce,ci,x;
    QP             qp;
    QPS            qps;
    PetscBool      converged;

    ierr = MatCreateBlockDiag(PETSC_COMM_WORLD,K,&Kbd);CHKERRQ(ierr);
    ierr = MatCreateBlockDiag(PETSC_COMM_WORLD,R,&Rbd);CHKERRQ(ierr);
    ierr = MatCreateVecs(Kbd,&x,NULL);CHKERRQ(ierr);
    ierr = MatTranspose(Be,MAT_INITIAL_MATRIX,&Bet);CHKERRQ(ierr);
    ierr = MatDestroy(&Be);CHKERRQ(ierr);
    ierr = MatTranspose(Bi,MAT_INITIAL_MATRIX,&Bit);CHKERRQ(ierr);
    ierr = MatDestroy(&Bi);CHKERRQ(ierr);
    ierr = MatCreateVecs(Bet,NULL,&ce);CHKERRQ(ierr);
    ierr = MatCreateVecs(Bit,NULL,&ci);CHKERRQ(ierr);
    ierr = VecZeroEntries(ce);CHKERRQ(ierr);
    ierr = VecZeroEntries(ci);CHKERRQ(ierr);

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    * Setup QP: argmin 1/2 x'Ax -x'b s.t. c <= I*x
    *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    ierr = QPCreate(PETSC_COMM_WORLD,&qp);CHKERRQ(ierr);
    /* Set matrix representing QP operator */
    ierr = QPSetOperator(qp,Kbd);CHKERRQ(ierr);
    /* Set right hand side */
    ierr = QPSetRhs(qp,F);CHKERRQ(ierr);
    /* Set initial guess.
    * THIS VECTOR WILL ALSO HOLD THE SOLUTION OF QP */
    ierr = QPSetInitialVector(qp,x);CHKERRQ(ierr);
    ierr = QPSetEq(qp,Bet,ce);CHKERRQ(ierr);
    /* Set inequality constraint c <= Bx in the form -c >= -Bx*/
    if (!linear) {
      ierr = QPSetIneq(qp,Bit,ci);CHKERRQ(ierr);
    } else {
      ierr = QPAddEq(qp,Bit,ci);CHKERRQ(ierr);
    }
    ierr = QPSetOperatorNullSpace(qp,Rbd);CHKERRQ(ierr);

    /* FETI */
    ierr = PetscOptionsInsertString(NULL,"-feti");CHKERRQ(ierr);
    ierr = PetscLogStagePush(stage_transform);CHKERRQ(ierr);
    ierr = QPTFromOptions(qp);CHKERRQ(ierr);
    ierr = PetscLogStagePop();CHKERRQ(ierr);

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    * Setup QPS, i.e. QP Solver
    *   Note the use of PetscObjectComm() to get the same comm as in qp object.
    *   We could specify the comm explicitly, in this case PETSC_COMM_WORLD.
    *   Also, all PERMON objects are PETSc objects as well :)
    *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    ierr = QPSCreate(PetscObjectComm((PetscObject)qp),&qps);CHKERRQ(ierr);
    /* Set QP to solve */
    ierr = QPSSetQP(qps,qp);CHKERRQ(ierr);
    /* Set runtime options for solver, e.g,
    *   -qps_type <type> -qps_rtol <relative tolerance> -qps_view_convergence */
    ierr = QPSSetFromOptions(qps);CHKERRQ(ierr);

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    * Solve QP
    *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    ierr = PetscLogStagePush(stage_solve);CHKERRQ(ierr);
    ierr = QPSSolve(qps);CHKERRQ(ierr);
    ierr = PetscLogStagePop();CHKERRQ(ierr);

    /* Check that QPS converged */
    ierr = QPIsSolved(qp,&converged);CHKERRQ(ierr);
    if (!converged) PetscPrintf(PETSC_COMM_WORLD,"QPS did not converge!\n");
    ierr = VecViewFromOptions(x,PETSC_NULLPTR,"-view_sol");CHKERRQ(ierr);

    ierr = QPSDestroy(&qps);CHKERRQ(ierr);
    ierr = QPDestroy(&qp);CHKERRQ(ierr);
    ierr = VecDestroy(&x);CHKERRQ(ierr);
    ierr = VecDestroy(&ce);CHKERRQ(ierr);
    ierr = VecDestroy(&ci);CHKERRQ(ierr);
    ierr = MatDestroy(&Bet);CHKERRQ(ierr);
    ierr = MatDestroy(&Bit);CHKERRQ(ierr);
    ierr = MatDestroy(&Kbd);CHKERRQ(ierr);
    ierr = MatDestroy(&Rbd);CHKERRQ(ierr);
  }

  ierr = MatDestroy(&K); 	    CHKERRQ(ierr);
  ierr = MatDestroy(&R); 	    CHKERRQ(ierr);
  ierr = VecDestroy(&F); 	    CHKERRQ(ierr);

  PermonFinalize();
  return 0;
}

