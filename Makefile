# This is an example makefile to compile and link an application with PERMON.

ALL: ddHTFETI # default target

CFLAGS      = -I. # include current directory
CPPFLAGS    =     # flags for cpp/cxx files
CLEANFILES  = ddHTFETI # delete executable with make clean

include ${PERMON_DIR}/lib/permon/conf/permon_base # rules and variables

ddHTFETI: ddHTFETI.o
	-${CLINKER} -o ddHTFETI ddHTFETI.o ${PETSC_TAO_LIB} ${PERMON_LIB}
	${RM} ddHTFETI.o

# use the following example for program consisting of multiple files
#ddHTFETI: ddHTFETI.o file2.o
#	-${CLINKER} -o ddHTFETI ddHTFETI.o file2.o ${PETSC_TAO_LIB} ${PERMON_LIB}
#	${RM} ddHTFETI.o file2.o

